<?php
    require_once 'class/Profesor.php';

    if (!isset($_SESSION['username']) || ($_SESSION['role'] != 0 )) {
        header('Location: index.php');
    }
    $username = $_SESSION['username'];
    $role = $_SESSION['role'];
    $profesor = new Profesor($username, $role);

    if (isset($_POST['studentId'], $_POST['grade'])) {
        $studentId = $_POST['studentId'];
        $grade = $_POST['grade'];
        if ($profesor->addGrade($studentId, $grade, $username)) {
            echo $profesor->getMessage();
        } else {
            echo $profesor->getMessage();
        }
    }
?>
<html>
    <head></head>
    <title>Profesor</title>
    <body>
        <h3>Logat ca si : <?php echo $profesor->getUsername(); ?> </h3> 
        <br/>
        <form method="post">
            <label>Selectati un student</label>
            <select name="studentId">
                <?php $profesor->printStudents(); ?>
            </select>
            <input
                type ="number" min="1" max="10" name="grade">
            <input type="submit" value="Add">
        </form>
    </body>
</html>

