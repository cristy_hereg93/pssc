<!DOCTYPE HTML>
<html>
    <head>
        <title>Secretariat</title>
    </head>
    <body>
        <h2>
            Logged in as : <?php echo $secretar->getUsername(); ?> 
        </h2> 
        <br>
        
        <form method="post">
            <input type="text" name="firstName" placeholder="First Name">
            <input type="text" name="lastName" placeholder="Last Name">
            <select name="faculty">
                <option value="AC">Automatica si Calculatoare</option>
                <option value ="ETC">Electronica si Telecomunicatii</option>
            </select>
            <input type="submit" value="Add">
        </form>
        
        <br>
        
        <form method="post">
            <input type="text" name="genDormList" placeholder="." hidden>
            <input type="submit" method="post" value="Generate Dorm List">
        </form>
       
        <br>
        
        <table>
            <tr>
                <th>Nume</th>
                <th>Prenume</th>
                <th>Facultatea</th>
                <th>Media generala</th>
                <?php $secretar->scholarshipList() ?>
            </tr>
        </table>
    </body>