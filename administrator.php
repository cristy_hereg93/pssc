<?php
    require_once 'class/Administrator.php';

    if (!isset($_SESSION['username']) || ($_SESSION['role'] != 2 )) {
        header('Location: index.php');
    }
    $username = $_SESSION['username'];
    $role = $_SESSION['role'];
    $administrator = new Administrator($username, $role);
?>
<!DOCTYPE HTML>

<html>
    <head>
        <meta charset="utf-8">
        <title>Administrator</title>
    </head>
    
    <body>
        <h2>
            Logat ca si: <?php echo $administrator->getUsername();?>
        </h2>
        
        <br>
        
        <table>
            <tr>
                <th>Prenume</th>
                <th>Nume</th>
                <th>Facultate</th>
                <th>Media generala</th>
                <?php $administrator->getDormList();?>
            </tr>
        </table>
    </body>
</html>