<?php

    require_once 'Base.php';

    class Login extends Base {

        private $username;
        private $password;
        private $error;
        public function __construct($user, $pass) {
            $this->username = $user;
            $this->password = $pass;
        }

        private function check_user() {
        
            $query = "SELECT * from `members` where `username` = '" . $this->username . "' and `password` = '" . $this->password . "'";
            $result = $this->Query($query, 'select');

            if ($this->row_numbers($result) == 1){
                $role = $this->fetch_result($result);
                $_SESSION['role']=$role[0]['role'];
                return true;
            }
            else{
                return false;
            }
    }
    
        public function getError(){
            return $this->error;
        }

        private function setError($message){
            $this->error = $message;
        }

        public function login() {
            if ($this->check_user()) {
                $_SESSION['username'] = $this->username;
                return true;
            } else {
                $this->setError('Parola sau username-ul nu sunt valide!');
                return false;
            }
        }
}

