<?php

    require_once 'User.php';
    class Administrator extends User {
        public function __construct($username, $role) {
            parent::__construct($username, $role);
        }
        
        public function getDormList(){
            $query = "SELECT *from `dormList`";
            $result = $this->Query($query, 'select');
            $rows = $this->fetch_result($result);
            foreach ($rows as $student) {
                echo '<tr>';
                echo "<td>{$student['firstName']}</td>" .
                     "<td>{$student['lastName']}</td>" .
                     "<td>{$student['faculty']}</td>" .
                     "<td>{$student['average']}</td>";
                echo '</tr>';
            }
        }
    }