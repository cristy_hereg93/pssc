<?php
    
    require_once 'Base.php';
    
    class User extends Base{
        private $username;
        private $role;
        private $message;
        
        public function __construct($username, $role) {
            $this->username = $username;
            $this->role = $role;
        }
        
        public function getMessage(){
            return $this->message;
        }
        
        protected function setMessage($message){
            $this->message = $message;
        }
        
        public function getUsername(){
            return $this->username;
        }
        
        public function getRole(){
            return $this->role;
        }
        
        public function printStudent(){
            $query = "SELECT *from `student`";
            $rows = $this->Query($query, 'select');
            $result = $this->fetch_result($rows);
            return $result;
        }
    }

