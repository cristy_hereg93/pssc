<?php

    require_once 'config.php';

    class Base {

        private $mysqli;

        public function __construct() {
        }

        private function connect() {
            $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die(mysqli_error($mysqli));
            $this->mysqli = $mysqli;
        }

        protected function Query($query, $type) {
            $this->connect();

            if ($type == 'insert') {
                mysqli_query($this->mysqli, $query) or die(mysqli_error($this->mysqli));
                return true;
            } else if ($type = 'select') {
                $result = mysqli_query($this->mysqli, $query) or die(mysqli_error($this->mysqli));
                return $result;
            }
            return false;
        }

        protected function fetch_result($result) {
            while ($row = mysqli_fetch_array($result)){
                $rows[] = $row; 
            }
            return $rows;
        }

        protected function row_numbers($result) {
            return mysqli_num_rows($result);
        }
    }