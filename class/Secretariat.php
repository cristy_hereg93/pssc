<?php

    require_once 'User.php';

    class Secretariat extends User {

        private $minGrade;
        private $dormGrade;
        
        //Overrides method from User.php
        public function __construct($username, $role) {
            parent::__construct($username, $role);
        }

        public function getMinGrade() {
            return $this->minGrade;
        }

        public function setMinGrade($min) {
            $this->minGrade = $min;
        }

        public function getDormGrade() {
            return $this->dormGrade;
        }

        public function setDormGrade($min) {
            $this->dormGrade = $min;
        }

        public function addStudent($firstName, $lastName, $faculty) {
            $query = "INSERT INTO `student`(firstName,lastName,faculty) VALUES" .
                     "('$firstName','$lastName','$faculty')";
            if ($this->Query($query, 'insert')) {
                $this->setMessage('Student created successfully');
                return true;
            } else {
                $this->setMessage('Error');
                return false;
            }
        }

        private function average($studentId) {
            $query = "SELECT `grade` FROM `grades` WHERE `studentId`={$studentId}";
            $rows = $this->Query($query, 'select');
            $results = $this->fetch_result($rows);
            $average = 0;
            foreach ($results as $value) {
                $average+=$value['grade'];
            }
            $average = $average / $this->row_numbers($rows);
            return $average;
        }

        public function scholarshipList() {
            $students = $this->printStudents();
            foreach ($students as $student) {
                $studentId = $student['id'];
                if ($this->average($studentId) >= $this->minGrade) {
                    echo '<tr>';
                    echo "<td>{$student['firstName']}</td>" .
                         "<td>{$student['lastName']}</td>" .
                         "<td>{$student['faculty']}</td>" .
                         "<td>{$this->average($studentId)}</td>";
                    echo '</tr>';
                }
            }
        }

        public function genDormList() {
            $query = "DELETE from `dormList`";
            $this->Query($query, 'insert');
            $students = $this->printStudents();

            foreach ($students as $student) {
                $studentId = $student['id'];
                if ($this->average($studentId) >= $this->getDormGrade() && $this->average($studentId) != NULL){
                    $query = "INSERT INTO `dormList` (firstName,lastName,faculty,average) VALUES".
                             "('{$student['firstName']}','{$student['lastName']}" .
                             "','{$student['faculty']}','{$this->average($studentId)}')";
                }
                if ($this->Query($query, 'insert')) {
                    $this->setMessage('Lista locuri camin generata.');
                } else {
                    $this->setMessage('Eroare la generarea listei');
                }
            }
        }
}
